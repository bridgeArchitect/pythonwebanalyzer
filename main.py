#!/usr/bin/python3
import urllib.request
import smtplib
from time import sleep
import datetime

# constants for working
urlAddress = "http://91.202.128.107:55555/6D/L4Y/index.html"
searchRow = "Нехай це свято наповнить ваші серця добром"
findRow = "Квітень 29,"
from_addr = "tomilo.art.2016@gmail.com"
to_addr = "tomilo.art.forscience@gmail.com"
subject = "WebApp"
password = "Moskow2012"
errfile = "/home/bridgearchitect/SP/Lab6/ServiceWeb/error.log"
messfile = "/home/bridgearchitect/SP/Lab6/ServiceWeb/mess.log"
time = 10

def writeMessageEmail(message):

    # create smtp object
    smtpobj = smtplib.SMTP('smtp.gmail.com', 587)
    smtpobj.ehlo()
    smtpobj.starttls()

    # login to the email
    smtpobj.login(from_addr, password)

    # create message
    letter = "\r\n".join((
        "From: %s" % from_addr,
        "To: %s" % to_addr,
        "Subject: %s" % subject,
        "",
        message
    ))

    # send mail
    smtpobj.sendmail(from_addr, to_addr, letter)
    smtpobj.quit()

def writeMessageLog(message):

    # write message into logfile
    with open(messfile, "a+") as filelog:
        filelog.write(str(datetime.datetime.now()) + ":" + message + "\n")

def writeErrorLog(message):

    # write message into logfile
    with open(errfile, "a+") as filelog:
        filelog.write(str(datetime.datetime.now()) + ":" + message + "\n")

def analyze():

    with urllib.request.urlopen(urlAddress) as url:

        # read source
        source = url.read()

        # convert bytes array to string
        source = str(source, 'utf-8')

        # check existence of line
        if source.find(searchRow) == -1:
            writeMessageEmail("The necessary row is disappeared!")
            writeMessageLog("The necessary row is disappeared!")

        # check existence of additional line (for year)
        place = source.find(findRow)
        if place != -1:
            # receive year of event
            year = int(source[place + 12: place + 16])
            # check if this year multiples of 3
            if year % 3 != 0:
                writeMessageEmail("Year does not multiple of 3!")
                writeMessageLog("Year does not multiple of 3!")

# infinity loop
while True:
    try:
        # make analysis
        analyze()
    except Exception as ex:
        # handle exceptions
        writeErrorLog(str(ex))
    # sleep process
    sleep(time)